from copy import copy


class Profile:
    table_name = "profiles"

    id = None
    name = None

    def __init__(self, id=None, name=None):
        self.id = id
        self.name = name

    def from_dict(self, dict, return_copy=False):
        if dict is not None:
            self.__dict__.update(dict)
            if return_copy:
                return copy(self)

    def set_properties(self, values):
        self.id = values[0]
        self.name = values[1]
