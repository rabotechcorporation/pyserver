from copy import copy


class User:
    table_name = "users"

    id = None
    username = None
    password = None

    def __init__(self, id=None, username=None, password=None):
        self.id = id
        self.username = username
        self.password = password

    def from_dict(self, dict, return_copy=False):
        if dict is not None:
            self.__dict__.update(dict)
            if return_copy:
                return copy(self)

    def set_properties(self, values):
        self.id = values[0]
        self.username = values[1]
        self.password = values[2]

