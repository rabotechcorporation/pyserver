from copy import copy


class ProfileUser:
    table_name = "profile_user"

    id = None
    user_id = None
    profile_id = None

    def __init__(self, id=None, user_id=None, profile_id=None):
        self.id = id
        self.user_id = user_id
        self.profile_id = profile_id

    def from_dict(self, dict, return_copy=False):
        if dict is not None:
            self.__dict__.update(dict)
            if return_copy:
                return copy(self)

    def set_properties(self, values):
        self.id = values[0]
        self.user_id = values[1]
        self.profile_id = values[2]

