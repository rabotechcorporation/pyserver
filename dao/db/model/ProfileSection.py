from copy import copy


class ProfileSection:
    table_name = "profile_section"

    id = None
    profile_id = None
    section_id = None

    def __init__(self, id=None, profile_id=None, section_id=None):
        self.id = id
        self.profile_id = profile_id
        self.section_id = section_id

    def from_dict(self, dict, return_copy=False):
        if dict is not None:
            self.__dict__.update(dict)
            if return_copy:
                return copy(self)

    def set_properties(self, values):
        self.id = values[0]
        self.profile_id = values[1]
        self.section_id = values[2]
