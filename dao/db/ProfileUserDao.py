from BaseDao import DbBaseDao
from utils.Logger import Logger
import  json


class ProfileUserDao(DbBaseDao):
    logger = None
    table_name = None

    def __init__(self, model):
        self.logger = Logger.get_instance().logger
        DbBaseDao.__init__(self, model)
        self.table_name = model.table_name

    def add(self, cursor, params):
        if params[2] is not None:
            for param in params[3].split(','):
                query = self.get_query('add', (params[2], param))
                cursor.execute(query)

    def delete_by_user_id(self, cursor, params):
        query = self.get_query('delete_by_user', (params[2],))
        cursor.execute(query)
        return None
