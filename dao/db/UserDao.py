from BaseDao import DbBaseDao
from utils.Logger import Logger
from dao.db.model.ProfileUser import ProfileUser
from dao.db.ProfileUserDao import ProfileUserDao


class UserDao(DbBaseDao):
    logger = None
    table_name = None
    profile_user_dao = None

    def __init__(self, model):
        self.logger = Logger.get_instance().logger
        DbBaseDao.__init__(self, model)
        self.table_name = model.table_name
        self.profile_user_dao = ProfileUserDao(ProfileUser)

    def update(self, con, params):
        cursor = con.cursor()
        self.profile_user_dao.delete_by_user_id(cursor, params)
        self.profile_user_dao.add(cursor, params)
        query = self.get_query('update', params)
        cursor.execute(query)
        con.commit()
        cursor.close()

        return None

    def find_by_username(self, con, username, as_dict=False):
        return self.execute_query(con, 'find_by_username', (username,), as_dict=as_dict)
