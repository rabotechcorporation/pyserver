from BaseDao import DbBaseDao
from utils.Logger import Logger


class ProfileSectionDao(DbBaseDao):
    logger = None
    table_name = None

    def __init__(self, model):
        self.logger = Logger.get_instance().logger
        DbBaseDao.__init__(self, model)
        self.table_name = model.table_name

    def save_section(self, con, id, params):
        cursor = con.cursor()
        self.delete_sections(cursor, id)
        if params is not None and len(params) > 0:
            for param in params:
                query = self.get_query('add', param)
                cursor.execute(query)
        con.commit()

    def delete_sections(self, cursor, id):
        query = self.get_query('delete_by_profile', id)
        cursor.execute(query)
