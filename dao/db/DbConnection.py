
import psycopg2

from config.Config import Config
from utils.Logger import Logger


class DbConnection:
    logger = None
    __instance = None
    __con = None

    def __init__(self):
        self.logger = Logger.get_instance().logger

        if DbConnection.__instance is not None:
            raise Exception("This class is a singleton! Call get_instance")
        else:
            self.init_connection()

    def init_connection(self):
        try:
            connection_config = Config.get_instance().get_db_config()
            self.__con = psycopg2.connect("dbname='connection_manager' user='postgres' host='localhost' password='root'")
            DbConnection.__instance = self
        except Exception as err:
            self.logger.critical(err)
            quit()

    @staticmethod
    def get_connection():
        if DbConnection.__instance is None:
            DbConnection()

        if DbConnection.__con is None:
            DbConnection.__instance.init_connection()

        return DbConnection.__instance.__con

    @staticmethod
    def close_connection():
        if DbConnection.__instance is not None and DbConnection.__instance.__con is not None:
            DbConnection.__instance.__con.close()
