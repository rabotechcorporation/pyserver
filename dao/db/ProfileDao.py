from BaseDao import DbBaseDao
from utils.Logger import Logger


class ProfileDao(DbBaseDao):
    logger = None
    table_name = None

    def __init__(self, model):
        self.logger = Logger.get_instance().logger
        DbBaseDao.__init__(self, model)
        self.table_name = model.table_name
