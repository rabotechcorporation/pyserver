import json
import os

from utils.Logger import Logger


class DbBaseDao(object):
    queries = None
    table_name = None
    model = None

    def __init__(self, model):
        self.logger = Logger.get_instance().logger
        with open(os.path.dirname(os.path.abspath(__file__)) + '/../querys/postgres_queries.json') as queries_file:
            self.queries = json.load(queries_file)

        self.table_name = model.table_name
        self.model = model()

    def get_query(self, query, params=None):
        query_key = "query_" + query
        if self.table_name not in self.queries:
            raise Exception("Table not exist")
        if query_key not in self.queries[self.table_name]:
            raise Exception("Query not exist")

        db_query = self.queries[self.table_name][query_key]
        if params is not None and len(params) > 0:
            str_db_query = json.dumps(db_query)
            try:
                count = 0
                for param in params:
                    str_db_query = str_db_query.replace("{" + str(count) + "}", str(param))
                    count += 1
            except Exception as err:
                print(err)
            return json.loads(str_db_query, strict=False)
        else:
            return db_query

    def execute_query(self, con, query_name, params=None, as_dict=False):
        query = self.get_query(query_name, params)
        cursor = con.cursor()
        cursor.execute(query)

        columns = [column[0] for column in cursor.description]
        results = []
        for row in cursor:
            dict_result = dict(zip(columns, row))
            if callable(getattr(self.model, "fix_json", None)):
                dict_result = self.model.fix_json(dict_result)

            if not as_dict:
                result_model = self.model.from_dict(dict_result, True)
                results.append(result_model)
            else:
                results.append(dict_result)

        return results

    def add(self, con, params):
        query = self.get_query('add', params)
        cursor = con.cursor()
        cursor.execute(query)
        con.commit()
        res = cursor.fetchall()
        cursor.close()

        return res

    def update(self, con, params):
        query = self.get_query('update', params)
        cursor = con.cursor()
        cursor.execute(query)
        con.commit()
        cursor.close()

        return None

    def delete(self, con, id):
        query = self.get_query('delete', (id,))
        cursor = con.cursor()
        cursor.execute(query)
        con.commit()
        cursor.close()

        return None

    def find_all(self, con, as_dict=False):
        return self.execute_query(con, 'find_all', as_dict=as_dict)

    def find_by_id(self, con, id, as_dict=False):
        return self.execute_query(con, 'find_by_id', (id,), as_dict=as_dict)
