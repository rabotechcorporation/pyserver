from dao.db.model.Profile import Profile
from services.BaseService import BaseService
from utils.Logger import Logger
from dao.db.model.ProfileSection import ProfileSection
from dao.db.ProfileSectionDao import ProfileSectionDao
from dao.db.DbConnection import DbConnection


class ProfileService(BaseService):
    profile_section_dao = None

    def __init__(self):
        super(ProfileService, self).__init__(Profile)
        self.logger = Logger.get_instance().logger
        self.profile_section_dao = ProfileSectionDao(ProfileSection)

    def list_profile_sections(self, id, as_dict=False):
        con = DbConnection.get_connection()
        try:
            return None, self.profile_section_dao.find_by_id(con, id, as_dict)
        except Exception as err:
            self.logger.error(err)
            return err.message, None
        finally:
            DbConnection.close_connection()

    def save_sections(self, id, params):
        con = DbConnection.get_connection()
        try:
            return None, self.profile_section_dao.save_section(con, id, params)
        except Exception as err:
            self.logger.error(err)
            return err.message, None
        finally:
            DbConnection.close_connection()
