from dao.db.model.User import User
from dao.db.UserDao import UserDao
from services.BaseService import BaseService
from utils.Logger import Logger
from dao.db.DbConnection import DbConnection


class UserService(BaseService):
    dao = None

    def __init__(self):
        super(UserService, self).__init__(User)
        self.logger = Logger.get_instance().logger
        self.dao = UserDao(User)
    #
    # def add(self, params):
    #     con = DbConnection.get_connection()
    #     try:
    #         return None, self.base_dao.add(con, params)
    #     except Exception as err:
    #         self.logger.error(err)
    #         return err.message, None
    #     finally:
    #         DbConnection.close_connection()
    #

    def update(self, params):
        con = DbConnection.get_connection()
        try:
            return None, self.dao.update(con, params)
        except Exception as err:
            self.logger.error(err)
            return err.message, None
        finally:
            DbConnection.close_connection()

    def by_username(self, username, as_dict=False):
        con = DbConnection.get_connection()
        try:
            return self.dao.find_by_username(con, username, as_dict)
        except Exception as err:
            self.logger.error(err)
            return err.message, None
        finally:
            DbConnection.close_connection()
