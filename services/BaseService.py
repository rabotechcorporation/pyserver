from dao.db.BaseDao import DbBaseDao
from dao.db.DbConnection import DbConnection
from utils.Logger import Logger


class BaseService(object):
    logger = None
    base_dao = None
    table_name = None

    def __init__(self, model):
        self.logger = Logger.get_instance().logger
        self.base_dao = DbBaseDao(model)
        self.table_name = model.table_name

    def add(self, params):
        con = DbConnection.get_connection()
        try:
            return None, self.base_dao.add(con, params)
        except Exception as err:
            self.logger.error(err)
            return err.message, None
        finally:
            DbConnection.close_connection()

    def update(self, params):
        con = DbConnection.get_connection()
        try:
            return None, self.base_dao.update(con, params)
        except Exception as err:
            self.logger.error(err)
            return err.message, None
        finally:
            DbConnection.close_connection()

    def delete(self, id):
        con = DbConnection.get_connection()
        try:
            return None, self.base_dao.delete(con, id)
        except Exception as err:
            self.logger.error(err)
            return err.message, None
        finally:
            DbConnection.close_connection()

    def find_all(self, as_dict=False):
        con = DbConnection.get_connection()
        try:
            return None, self.base_dao.find_all(con, as_dict)
        except Exception as err:
            self.logger.error(err)
            return err.message, None
        finally:
            DbConnection.close_connection()

    def find_by_id(self, id, as_dict=False):
        con = DbConnection.get_connection()
        try:
            return None, self.base_dao.find_by_id(con, id, as_dict)
        except Exception as err:
            self.logger.error(err)
            return err.message, None
        finally:
            DbConnection.close_connection()