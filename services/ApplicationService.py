import json
import os
from utils.Logger import Logger

from config.Config import Config


class ApplicationService:
    logger = None

    def __init__(self):
        self.logger = Logger.get_instance().logger

    def get_log_err(self):
        path = Config.get_instance().get_config_value('settings')['log_path']
        data = None

        try:
            log_files = os.listdir(path)
            if log_files[-1] != 'bicom.log':
                with open(path + log_files[-1], 'r') as log_file:
                    data = log_file.read()

            with open(path + 'bicom.log', 'r') as log_file:
                if data is not None:
                    data.join(log_file.read())
                else:
                    data = log_file.read()

            return None, data
        except IOError as err:
            self.logger.error(err)
            return err.message, None

    def get_translations_from_file(self):
        path = '.\\i18n\\'

        try:
            with open(path + 'lang_es.json', 'r') as i18n_file:
                data = json.load(i18n_file)

            return None, data
        except IOError as err:
            self.logger.error(err)
            return err.message, None
