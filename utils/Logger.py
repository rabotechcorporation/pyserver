import logging
import os
from logging.handlers import RotatingFileHandler
from config.Config import Config


class Logger:
    __instance = None
    logger = None

    @staticmethod
    def get_instance():
        if Logger.__instance is None:
            Logger()
        return Logger.__instance

    def __init__(self):
        # loggin_level_values = Config.get_instance().get_config_value("login_level_values")
        # loggin_level = loggin_level_values[Config.get_instance().get_config_value("login_level")]

        if Logger.__instance is not None:
            raise Exception("This class is a singleton! Call get_instance")
        else:
            Logger.__instance = self

        f = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

        log_path = Config.get_instance().get_config_value('settings')['log_path']
        if not log_path.strip()[-1] == '/' and not log_path.strip()[-1] == '\\':
            log_path += '/'
        if not os.path.exists(log_path):
            try:
                os.mkdir(log_path)
            except OSError as err:
                log_path = "./logs/"

        handler = RotatingFileHandler(log_path + 'bicom.log', maxBytes=1024*1024, backupCount=5)
        handler.setFormatter(logging.Formatter(f))
        logging.basicConfig(format=f, level=logging.ERROR)
        self.logger = logging.getLogger()
        self.logger.addHandler(handler)
