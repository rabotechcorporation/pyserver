import binascii

from Crypto.Cipher import AES
from base64 import b64decode
from base64 import b64encode
from Crypto import Random
from Crypto.Util import Counter


def dictarr_values_to_string(dict_arr):
    if dict_arr is None:
        return
    for dict in dict_arr:
        dict_values_to_string(dict)

    return dict_arr


def dict_values_to_string(dict_json):
    for key in dict_json:
        if isinstance(dict_json[key], dict):
            dict_values_to_string(dict_json[key])
        else:
            if dict_json[key] is not None:
                dict_json[key] = str(dict_json[key])
            else:
                dict_json[key] = ''

    return dict_json


def encrypt(raw):
    key = "Rs5T2Wj2OKrGvNmP"
    raw = _pad(raw)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    encrypted = b64encode(iv + cipher.encrypt(raw))
    return encrypted


def decrypt(enc):
    key = "Rs5T2Wj2OKrGvNmP"
    enc = b64decode(enc)
    iv = enc[:AES.block_size]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return _unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')


def _pad(s):
    return s + (16 - len(s) % 16) * chr(16 - len(s) % 16)


def _unpad(s):
    return s[:-ord(s[len(s)-1:])]
