import json
from services.ApplicationService import ApplicationService


class ApplicationController:
    app = None
    service = None

    def __init__(self, app):
        self.service = ApplicationService()
        self.app = app

        app.route('/log_err', 'GET', self.get_log)
        app.route('/translation', 'GET', self.get_translations)

    def get_log(self):
        err, data = self.service.get_log_err()

        data = data.decode('utf-8', 'ignore').encode("utf-8")
        
        json_resp = {
            "success": err is None,
            "data": data,
            "err": err
        }
        return json.dumps(json_resp)

    def get_translations(self):
        err, data = self.service.get_translations_from_file()

        json_resp = {
            "success": err is None,
            "data": data,
            "err": err
        }
        return json.dumps(json_resp)
