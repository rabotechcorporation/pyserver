import json

from bottle import request

from services.ProfileService import ProfileService
from utils import Utils


class ProfileController:
    app = None
    service = None

    def __init__(self, app):
        self.service = ProfileService()
        self.app = app

        app.route('/profile/list', 'GET', self.list_profiles)
        app.route('/profile/<id>', 'GET', self.get_by_id)
        app.route('/profile', 'POST', self.add_or_update)
        app.route('/profile/<id>', 'POST', self.add_or_update)
        app.route('/profile/delete/<id>', 'POST', self.delete)
        app.route('/profile/sections/<id>', 'GET', self.list_profile_sections)
        app.route('/profile/sections/<id>', 'POST', self.save_sections)
        # app.route('/getProfile', 'GET', this.getSessionProfile.bind(this.getSessionProfile))
        # app.route('/changePassword', 'POST', this.changePassword.bind(this.changePassword))
        # app.route('/profile/settings/<id>', 'GET', this.getProfileSettings.bind(this.getProfileSettings))
        # app.route('/profile/settings/save', 'POST', this.saveSettings.bind(this.saveSettings))
        # app.route('/profile/settings/delete/<id>', 'GET', this.deleteSetting.bind(this.deleteSetting))

    def list_profiles(self):
        err, data = self.service.find_all(as_dict=True)
        json_resp = {
            "success": err is None,
            "data": Utils.dictarr_values_to_string(data),
            "err": err
        }
        return json.dumps(json_resp)

    def get_by_id(self, id):
        if id is not None:
            err, data = self.service.find_by_id(id, as_dict=True)
        else:
            err = "No id"
            data = None

        json_resp = {
            "success": err is None,
            "data": Utils.dictarr_values_to_string(data),
            "err": err
        }
        return json.dumps(json_resp)

    def add_or_update(self, id=None):
        body = request.body.read()
        json_body = json.loads(body)

        name = json_body['name']

        if id is None:
            err, data = self.service.add((name,))
        else:
            err, data = self.service.update((name, id))

        json_resp = {
            "success": err is None,
            "data": data,
            "err": err
        }
        return json.dumps(json_resp)

    def delete(self, id):
        if id is not None:
            err, data = self.service.delete(id)
        else:
            err = "No id"
            data = None

        json_resp = {
            "success": err is None,
            "data": data,
            "err": err
        }
        return json.dumps(json_resp)

    def list_profile_sections(self, id):
        if id is not None:
            err, data = self.service.list_profile_sections(id, as_dict=True)
        else:
            err = "No id"
            data = None

        json_resp = {
            "success": err is None,
            "data": data,
            "err": err
        }
        return json.dumps(json_resp)

    def save_sections(self, id):
        body = request.body.read()
        json_body = json.loads(body)

        params = []

        for section in json_body:
            params.append((id, section))

        if id is not None:
            err, data = self.service.save_sections(id, params)
        else:
            err = "No id"
            data = None

        json_resp = {
            "success": err is None,
            "data": data,
            "err": err
        }
        return json.dumps(json_resp)
