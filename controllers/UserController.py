import json

from bottle import request, route
from beaker.middleware import SessionMiddleware

from services.UserService import UserService
from services.ProfileService import ProfileService
from utils import Utils


class UserController:
    app = None
    service = None
    profile_service = None
    
    def __init__(self, app):
        self.service = UserService()
        self.profile_service = ProfileService()
        self.app = app

        app.route('/user/list', 'GET', self.list_users)
        app.route('/user/<id>', 'GET', self.get_by_id)
        app.route('/user', 'POST', self.add_or_update)
        app.route('/user/<id>', 'POST', self.add_or_update)
        app.route('/user/delete/<id>', 'POST', self.delete)
        app.route('/getUser', 'GET', self.check_session)
        app.route('/login', 'GET', self.login)
        app.route('/login', 'POST', self.login_post)
        app.route('/logout', 'GET', self.logout)
        # app.route('/getUser', 'GET', this.getSessionUser.bind(this.getSessionUser))
        # app.route('/changePassword', 'POST', this.changePassword.bind(this.changePassword))
        # app.route('/user/settings/<id>', 'GET', this.getUserSettings.bind(this.getUserSettings))
        # app.route('/user/settings/save', 'POST', this.saveSettings.bind(this.saveSettings))
        # app.route('/user/settings/delete/<id>', 'GET', this.deleteSetting.bind(this.deleteSetting))

        session_opts = {
            'session.type': 'file',
            'session.data_dir': '/tmp/',
            'session.cookie_expires': True,
        }
        self.app = SessionMiddleware(self.app, session_opts)

    def list_users(self):
        err, data = self.service.find_all(as_dict=True)
        json_resp = {
            "success": err is None,
            "data": Utils.dictarr_values_to_string(data),
            "err": err
        }
        return json.dumps(json_resp)

    def get_by_id(self, id):
        if id is not None:
            err, data = self.service.find_by_id(id, as_dict=True)
        else:
            err = "No id"
            data = None

        json_resp = {
            "success": err is None,
            "data": Utils.dictarr_values_to_string(data),
            "err": err
        }
        return json.dumps(json_resp)

    def add_or_update(self, id=None):
        body = request.body.read()
        json_body = json.loads(body)
        profile_ids = None

        username = json_body['username']
        password = json_body['password']
        try:
            profile_ids = json_body['profileids']
        except KeyError:
            profile_ids = []

        if id is None:
            err, data = self.service.add((username, Utils.encrypt(password)))
        else:
            old_user = self.get_by_id(id)
            if json.loads(old_user)["data"][0]["password"] == password:
                err, data = self.service.update((username, password, id, profile_ids,))
            else:
                err, data = self.service.update((username, Utils.encrypt(password), id, profile_ids,))

        json_resp = {
            "success": err is None,
            "data": data,
            "err": err
        }
        return json.dumps(json_resp)

    def delete(self, id):
        if id is not None:
            err, data = self.service.delete(id)
        else:
            err = "No id"
            data = None

        json_resp = {
            "success": err is None,
            "data": data,
            "err": err
        }
        return json.dumps(json_resp)

    def get_by_username(self):
        username = None
        if username is not None:
            err, data = self.service.by_username(username)
            status = 200
        else:
            err = "No username"
            data = None
            status = 403

        json_resp = {
            "success": err is None,
            "status": status,
            "data": data,
            "err": err
        }
        return json.dumps(json_resp)

    def login(self):
        json_resp = {
            "success": False,
            "status": 403,
            "err": 'Unauthorized'
        }
        return json.dumps(json_resp)

    def login_post(self):
        body = request.body.read()
        json_body = json.loads(body)

        session = request.environ['beaker.session']

        username = json_body["user"]
        password = json_body["pass"]

        user = self.service.by_username(username)

        if len(user) == 0:
            err = 'Invalid username'
            data = None
        else:
            user = user[0]
            profile_sections = self.profile_service.list_profile_sections(user.profile_id, True)[1]
            sections = []
            for section in profile_sections:
                sections.append(section['section_id'])

            if password == Utils.decrypt(user.password):
                err = None
                data = {"id": user.id, "username": user.username, "name": user.profile_name, "sections": sections}
                session['logged_in'] = True
                session['user_id'] = user.id
                session['username'] = user.username
                session['profile_name'] = user.profile_name
                session['sections'] = sections
            else:
                err = 'Invalid password'
                data = None

        json_resp = {
            "success": err is None,
            "data": data,
            "err": err
        }
        return json.dumps(json_resp)

    def check_session(self):
        session = request.environ['beaker.session']
        err, data = None, None

        if 'logged_in' in session and session['logged_in'] is True:
            data = {"id": session['user_id'],
                    "username": session['username'],
                    "name": session['profile_name'],
                    "sections": session['sections']}
            json_resp = {
                "success": err is None,
                "data": data,
                "err": err
            }
            return json.dumps(json_resp)
        else:
            json_resp = {
                "success": False,
                "status": 403,
                "data": data,
                "err": 'Unauthorized'
            }
            return json.dumps(json_resp)

    def logout(self):
        session = request.environ['beaker.session']
        session['logged_in'] = False

        json_resp = {
            "success": True
        }
        return json.dumps(json_resp)
