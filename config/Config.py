import json
import os


class Config:
    __instance = None
    profile = None

    configuration = {}
    database_config = {}

    @staticmethod
    def get_instance(profile=None):
        if Config.__instance is None:
            Config(profile)
        return Config.__instance

    def __init__(self, profile):
        if profile is None:
            raise Exception("No profile selected")
        if Config.__instance is not None:
            raise Exception("This class is a singleton! Call get_instance")

        self.profile = profile
        self.load_config()


    def load_config(self):
        try:
            with open(os.path.dirname(os.path.abspath(__file__)) + '/profiles/app_conf/configuration_{0}.json'.format(self.profile)) as config_file:
                self.configuration = json.load(config_file)
                print('{0} configuration parameters loaded').format(str(len(self.configuration)))
            with open(os.path.dirname(os.path.abspath(__file__)) + '/profiles/db_conf/database_{0}.json'.format(self.profile)) as db_config_file:
                self.database_config = json.load(db_config_file)
                print('{0} configuration parameters loaded').format(str(len(self.database_config)))

            Config.__instance = self
        except IOError:
            print("Can't load configuration file")

    def get_all_configuration(self):
        return self.configuration

    def get_config_value(self, key):
        if key in self.configuration is not None:
            return self.configuration[key]
        else:
            print("Configuration param {0} not exist" ).format(key)

        return ""

    def get_connections(self):
        return self.configuration['connections']

    def get_connection(self, id):
        connections = self.configuration['connections']
        for con in connections:
            if con['id'] == id:
                return con

    def get_config_from_list(self, key, key_to_find, value_to_find):
        configs = self.configuration[key]
        for config in configs:
            if str(config[key_to_find]) == str(value_to_find):
                return config

    # database configuration
    def get_db_config(self):
        return self.database_config["connection_config"]

    def get_db_config_value(self, key):
        if key in self.database_config is not None:
            return self.database_config[key]
        else:
            print("Db Configuration param {0} not exist" ).format(key)

        return ""