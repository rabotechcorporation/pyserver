import importlib
import sys
import os
from os import listdir
from os.path import isfile
import inspect
import bottle
from bottle import Bottle, response, request
from config.Config import Config
from yoyo import read_migrations
from yoyo import get_backend
from beaker.middleware import SessionMiddleware

# cargar la consifuracion para el perfil
profile = "dev"
if len(sys.argv) > 1:
    profile = str(sys.argv[1])
Config.get_instance(profile)

session_opts = {
    'session.type': 'memory',
    'session.auto': True
}

bottle_app = Bottle()
controllers = []


def recursive_read_controllers_dir(path, module_name="controllers"):
    for f in listdir(path):
        if f == "__init__.py" or f.find('.pyc') != -1:
            continue

        if isfile(path + "/" + f):
            if module_name is None:
                module_name = "controllers"
            module_name += "." + f.replace(".py", "")
            controller = importlib.import_module(module_name)
            for class_obj in inspect.getmembers(controller, inspect.isclass):
                class_package = str(class_obj[1])
                if class_package[:class_package.rfind(".")] == module_name:
                    class_obj[1](bottle_app)
                    controllers.append(class_package)
                    if len(module_name.split(".")) > 2 != "controllers":
                        return "controllers"
                    else:
                        module_name = "controllers"
        else:
            module_name += "." + f
            module_name = recursive_read_controllers_dir(path + "/" + f, module_name)


@bottle_app.hook('after_request')
def enable_cors():
    if request.headers.environ['PATH_INFO'] != "/eppr/export_data":
        response.content_type = 'application/json'
    response.headers['Access-Control-Allow-Origin'] = request.headers.environ['HTTP_ORIGIN'] if 'HTTP_ORIGIN' in request.headers.environ else "*"
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Authorization, Origin, Accept, Content-Type, X-Requested-With'
    response.headers['Access-Control-Allow-Credentials'] = 'true'


@bottle_app.route('/', method='OPTIONS')
@bottle_app.route('/<path:path>', method='OPTIONS')
def options_handler(path=None):
    return


# migraciones bd
db_config = Config.get_instance().get_db_config()
backend = get_backend('postgresql://{0}:{1}@{2}/{3}'.format(db_config['user'], db_config['password'], db_config['host'], db_config['database']))
migrations = read_migrations(os.path.dirname(os.path.abspath(__file__)) + '/migrations')
with backend.lock():
    backend.apply_migrations(backend.to_apply(migrations))

# Cargar controladores
recursive_read_controllers_dir(os.path.dirname(os.path.abspath(__file__)) + '/controllers')

bottle_app = SessionMiddleware(bottle_app, session_opts)

# Iniciar bottle
bottle.run(app=bottle_app, server="paste", host=Config.get_instance().get_config_value('server')['host'], port=Config.get_instance().get_config_value('server')['port'], debug=True)


