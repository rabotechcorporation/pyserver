from yoyo import step

from migrations import MigrationUtils as utils


def migrate(conn):
    cursor = conn.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS public.users"
                   "("
                   "    id serial NOT NULL,"
                   "    username character varying(50) NOT NULL UNIQUE,"
                   "    password character varying(64) NOT NULL,"                 
                   "    CONSTRAINT users_pkey PRIMARY KEY (id)"
                   ")")
    cursor.close()

    cursor = conn.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS public.profiles"
                   "("
                   "    id serial NOT NULL,"
                   "    name character varying(50) NOT NULL UNIQUE,"
                   "    CONSTRAINT profiles_pkey PRIMARY KEY (id)"
                   ")")
    cursor.close()

    cursor = conn.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS public.profile_user"
                   "("
                   "    id serial NOT NULL,"                     
                   "    user_id integer NOT NULL,"
                   "    profile_id integer NOT NULL,"
                   "    CONSTRAINT profile_user_pkey PRIMARY KEY (id),"
                   "    CONSTRAINT fk_profile_user_user FOREIGN KEY (user_id)"
                   "        REFERENCES public.users (id) MATCH SIMPLE"
                   "        ON UPDATE NO ACTION"
                   "        ON DELETE CASCADE,"
                   "    CONSTRAINT fk_profile_user_profile FOREIGN KEY (profile_id)"
                   "        REFERENCES public.profiles (id) MATCH SIMPLE"
                   "        ON UPDATE NO ACTION"
                   "        ON DELETE NO ACTION"
                   ")")
    cursor.close()

    cursor = conn.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS public.profile_section"
                   "("
                   "    id serial NOT NULL,"
                   "    profile_id integer NOT NULL,"
                   "    section_id character varying(100) NOT NULL,"
                   "    CONSTRAINT profile_section_pkey PRIMARY KEY (id),"
                   "    CONSTRAINT fk_profile_section_profile FOREIGN KEY (profile_id)"
                   "        REFERENCES public.profiles (id) MATCH SIMPLE"
                   "        ON UPDATE CASCADE"
                   "        ON DELETE CASCADE"                       
                   ")")
    cursor.close()

    if not utils.check_value_exist(conn, "public.users", "id", 1, False):
        cursor = conn.cursor()
        cursor.execute("INSERT INTO public.users (username, password)"
                       " VALUES ('admin', 'R9eDB6oEdbzrWXHzLoRsWIk6bBnfRrQO7rtacfWTg+I=')")
        cursor.close()

    if not utils.check_value_exist(conn, "public.profiles", "id", 1, False):
        cursor = conn.cursor()
        cursor.execute("INSERT INTO public.profiles (name) VALUES ('Administrador')")
        cursor.close()

    if not utils.check_value_exist(conn, "public.profile_user", "id", 1, False):
        cursor = conn.cursor()
        cursor.execute("INSERT INTO public.profile_user (user_id,profile_id) VALUES (1,1)")
        cursor.close()

    if not utils.check_value_exist(conn, "public.profile_section", "section_id", "/configuration", True):
        cursor = conn.cursor()
        cursor.execute("INSERT INTO public.profile_section (profile_id,section_id) VALUES (1,'/configuration')")
        cursor.close()


step(migrate)
