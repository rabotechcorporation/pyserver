def check_table_exist(con, table_name):
    with con.cursor() as cursor:
        try:
            cursor.execute("select * from {0}".format(table_name))
            return True
        except Exception as err:
            print(err)
            return False


def check_value_exist(con, table_name, column, value, as_string=True):
    with con.cursor() as cursor:
        try:
            if as_string:
                cursor.execute("select * from {0} where {1}='{2}'".format(table_name, column, value))
            else:
                cursor.execute("select * from {0} where {1}={2}".format(table_name, column, value))

            res = cursor.fetchall()
            return res is not None and len(res) > 0
        except Exception as err:
            print(err)
            return False
